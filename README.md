# Manage Adblock Plus build resources via Ansible

This is an [Ansible Role][role] for setting up an enviroment, where Adblock
Plus can be tested and built in an automated fashion.

[role]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html
